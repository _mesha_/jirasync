<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('commands.index');
});

Route::get('/commands/', function () {
    return view('commands.index');
});

Route::get('/issue/{key}', function () {
    $jm = new \App\Jira\Mananger();
    dd($jm->getIssue(request()->key));
});

Route::get('/issue/transition/{key}', function () {
    $jm = new \App\Jira\Mananger();
    dd($jm->getTransitions(request()->key));
});

Route::get('/hello', function() {
    return view('vendor.plugin.hello');
});