<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/
$jm = new App\Jira\Mananger();
use App\Jira\Status;
use App\Jira\Transition;
use App\Calling\Status as CallingStatus;
use App\Jira\Project;
use App\Calling\Calling;
use App\Calling\CallingType;

Artisan::command('issue:create', function () use ($jm) {

    $this->info('Initial issue charge');
    $this->comment('Can duplicate issues if this command has already been run.');

    $created = App\Services\CallingService::createIssue(); 

    $this->table(
      ['Calling', 'Task'],
      $created
    );
    
});

Artisan::command('issue:update', function () use ($jm) {

  $this->info('[' . date('H:i:s') . '] Init issue update');

  $res = \DB::table('callings')->where('needs_update', true)->get();

  $res->each(function($call) use ($jm) {

    $issue = \App\Jira\Issue::callingNumber($call->num_chamado)->first();    
    $jm->updateIssue($issue, $call);

  });
  
})->describe('Update calling issues in a project');

Artisan::command('issue:status', function () use ($jm) {

  $this->info('Initial statuses charge');

  $statuses = $jm->getStatues();
  
  $latualizados = [];
  
  foreach($statuses as $status) {

    $status = \App\Jira\Status::create([
      'name' => $status['name'],
      'jira_id' => $status['id'],
      'self' => $status['self']
    ]);

    if ($status)
      $linseridos[] = [$status['name'], $status['id'], $status['self']];
  }

  $this->table(
    ['name', 'Jira:id', 'self'],
    $linseridos
  );
  
});

Artisan::command('issue:transitions', function () use ($jm) {

  $this->info('Initial issue transition charge');

  $jm = new \App\Jira\Mananger();

  $transitions = $jm->getTransitions('OD-1');
  $latualizados = [];

  foreach($transitions['transitions'] as $transition) {

    $status = \App\Jira\Status::whereName($transition['name'])->first();
    
    if ($status) {

      $status->transition_id = $transition['id'];
      $status->save();

      $latualizados[] = [$status->name, $status->transition_id];
    }

  }

  $this->table(
    ['Status', 'Transition:ID'],
    $latualizados
  );
  
});