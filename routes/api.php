<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    
});

Route::post('/callings', function(Request $request) {
  
    $jm = new App\Jira\Mananger();

    return $jm->createIssue([
        "fields" => [
            'project' => [
                'id'=>$jm->project->jira_id
            ],
            'summary' => $request->num_chamado . ' ' . $request->titulo,
            "description" => $jm->removeSpecialCharacters($request->descricao),
            "issuetype" => [
                "id" => "10002" // task that's need to be done
            ]
        ]
    ]);
});

Route::put('/callings', function(Request $request) {
  
    $jm = new App\Jira\Mananger();
    $jm->updateIssue($request->all());
});

Route::post('/project_created', function (Request $request) {
    \App\Jira\Project::create([
        "self" => $request->project['self'],
        "jira_id" => $request->project['id'],
        "key" => $request->project['key'],
        "name" => $request->project['name'],
        "avatar_url"=>$request->project["avatarUrls"]["48x48"]
    ]);
});

Route::get('callings/', 'CallingController@index');

Route::post('/project_deleted', function (Request $request) {
    \App\Jira\Project::where('jira_id', $request->project['id'])->delete();
});

Route::resource('jira/statuses', 'Jira\StatusController')->except(['create', 'edit']);
Route::resource('jira/projects', 'Jira\ProjectController')->only(['index', 'show']);
Route::resource('jira/issuetype', 'Jira\IssueTypeController')->only(['index']);

Route::get('jira/transitions', function() {
    $jm = new App\Jira\Mananger();
    dd($jm->getTransitions('OD-1'));
});

Route::get('jira/issue/{key?}', function() {
    $key = request()->key;
    $jm = new App\Jira\Mananger();
    return response()->json($jm->getIssue($key), 200);
});

Route::post('issue/create', function(){
    $service = new App\Services\IssueService;
    $created = $service->createIssue(); 
    return response()->json($created, 200);
});

Route::get('inspire', function() {
    return Illuminate\Foundation\Inspiring::quote();
});