<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{csrf_token()}}">

        <title>{{__(config('app.name'))}}</title>

        <!-- Fonts -->
        <link href="{{mix('css/app.css')}}" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #f5f5f5;
            }
        </style>
    </head>
    <body>
        <div class="row">
            <div class="col-md-12">
                @include('partials.navbar')
            </div>
            <div class="col-md-12">
                <div class="container" style="padding-top: 3em">
                    <div class="row" id="app" v-cloak>
                        @yield('content')
                    </div>
                </div>  
            </div>
        </div>

        <script src="{{mix('js/app.js')}}"></script>

        <!-- Scripts -->
        @stack('pageLevelScripts')
    </body>
</html>
