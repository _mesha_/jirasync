@extends('layouts.app')
@section('content')
<div class="col-md-2"></div>
<div class="col-md-8">
    <div class="card text-white bg-primary mb-3">
        <div class="card-header">
            Activities Log
        </div>
        <div class="card-body" style="height: 70vh; overflow-y: auto">
            <table class="table text-white ">
                <thead>
                    <tr>
                        <th>Descrição</th>
                        <th>Data</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach(getlog(10) as $log)
                    <tr>
                        <td style="width: calc(70%)"><small>{{$log->description}}</small></td>
                        <td style="width: calc(30%)">{{$log->created_at->format('d/m/Y H:i:s')}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="col-md-2"></div>
@stop