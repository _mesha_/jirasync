@extends('plugin::layout')

@section('content')
<style>
    .table tbody tr td {
        border: 1px solid gray;
    }
</style>

    <div class="aui-page-panel">
        <section class="aui-page-panel-content">
            <h1>Congratulations, Add-on works!</h1>
            <p>
                You are authenticated using <strong>JWT</strong> to tenant with <strong>ID {{ Auth::id() }}</strong>
            </p>
            <p>
                That's up to you.
            </p>
        </section>
    </div>

    <div class="aui-page-panel">
        <section class="aui-page-panel-content">
            <h1>Last inserted Issues</h1>
            <table class="table">
                <thead>
                    <tr>
                        <th>Nº</th>
                        <th>Title</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach(\App\Calling\Calling::limit(10)->get() as $issue)
                    <tr>
                        <td>
                            {{$issue->num_chamado}}
                        </td>
                        <td>
                            {{$issue->titulo}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </section>
    </div>
@endsection