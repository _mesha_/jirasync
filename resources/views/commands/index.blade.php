@extends('layouts.app')
@section('content')
<div class="col-md-12">
	<div class="terminal">
		<div class="terminal-nav">
			<i class="fa fa-circle text-danger"></i>
			<i class="fa fa-circle text-success"></i>
			<i class="fa fa-circle text-warning"></i>
		</div>
		<div class="terminal-body" id="terminal">
			<span>Hello!</span> <br>
			<i class="fa fa-quote-left"></i> <small id="quote"><i>@{{inspire}}</i></small> <i class="fa fa-quote-right"></i>
			<br />
			<vue-typed-js :strings="['Let\'s type some commands to Jira', 'starts with command:list to know the list of commands available']" :show-cursor="false">
				<span class="text-muted typing"></span>
			</vue-typed-js>			<template v-for="message in display">
				<span class="text-success"><strong>~</strong></span>
				<span :class="'text-' + message.type" v-if="!isHtml(message.text)">
						@{{message.text}}
				</span>
				<span v-else v-html="message.text"></span>
				<br />
			</template>
			<span class="text-muted" v-if="loading">wait ...</span>
			<input type="text" class="terminal-control" autofocus="" v-model="command" id="prompt" @keyup.enter="execute()" @keyup.up="scrollCommands()" :disabled="loading">
		</div>
	</div>
</div>
@stop
@push('pageLevelScripts')
<script>
	new Vue({
		el:'#app',
		data: {
			inspire: '',
			loading: false,
			callings: [],
			command: '',
			display: [],
			vector: 0,
			executeds: [],
			commands: [
				{
					signature: 'issue:create',
					method: 'createIssues',
					description: 'create new issues on jira'
				},
				{
					signature: 'issue:update',
					method: 'updateIssues',
					description: 'update already created issues on jira'
				},
				{
					signature: 'issue:type',
					method: 'getIssueType',
					description: 'Get the issue types'
				},
				{
					signature: 'calling:fetch',
					method: 'fetchNewCallings',
					description: 'fetch new callings'
				},
				{
					signature: 'command:list',
					method: 'listCommands',
					description: 'lists all commands'
				},
				{
					signature: 'clear',
					method: 'clear',
					description: 'clear screen'
				},
				{
					signature: 'jira:projects',
					method: 'listProjects',
					description: 'List projects'
				},
				{
					signature: 'jira:project{key}',
					method: 'listProjects',
					description: 'List projects'
				}
			],
			source: ''
		},
		created() {
			const CancelToken = axios.CancelToken;
			this.source = CancelToken.source();
			axios.get('api/inspire').then( res => {
				this.inspire = res.data;
			});
		},
		methods: {
			fetchNewCallings() {
				axios.get('api/callings').then( res => {
					this.loading = false;
					if (res.data.length > 0) {
						this.callings = res.data;
						this.displayNewCallings(res.data);
					}
				});
			},
			displayNewCallings(calligns) {
				let str = "<br /><span class='text-muted'>🚀 fetching callings ...</span> <br />";
				str += `<span class='text-success'>${this.callings.length} new callings found:</span><br />`;
				str += '<br />';
				str += this.tableCallings(calligns);
				this.display.push({
					type: 'default',
					text: str
				})
			},
			tableCallings(callings) {
				let table = "<table class='table table-condensed table-dashed text-white'><thead><tr><th>Nº</th><th>Titulo</th></thead><tbody>";
				for (var i in callings) {
					let calling =  callings[i];	
					table += `<tr><td>${calling.num_chamado}</td><td>${calling.titulo} (${calling.tipo})</td></tr>`;
				}

				table += '</tbody></table>';

				return table;
			},
			tableCommands() {
				let table = "<table class='table table-condensed table-dashed text-white'><thead><tr><th>Signature</th><th>Description</th></thead><tbody>";
				for (var i in this.commands) {
					let command =  this.commands[i];	
					table += `<tr><td>${command.signature}</td><td>${command.description}</td></tr>`;
				}

				table += '</tbody></table>';

				return table;
			},
			execute() {
				let command = this.getAvailableCommand(this.command);
				
				if (command) {
					this.display.push({
						type: 'default',
						text: this.command
					});

					this.loading = true;
					let param = this.getParam(this.command);
					this[command.method](param);

				} else {
					this.display.push({
						type: 'muted',
						text: `terminal: command not found: ${this.command} `,
					});
				}

				this.executeds.push(this.command);
				this.command = "";

				this.scrollDown()
			},
			scrollDown() {
				var objDiv = document.getElementById("terminal");
				objDiv.scrollTop = objDiv.scrollHeight;
			},
			getParam(param) {
				var arr = param.match(new RegExp("{.*}"));
				if (Array.isArray(arr)) {
					return arr[0].replace('{', '').replace('}', '');
				}
			},
			getAvailableCommand(command) {
				var param = this.getParam(command);
				
				if (param)
					command = command.replace(param, 'key');

				for (var i in this.commands) {
					if (this.commands[i].signature === command)
						return this.commands[i];
				}

				return false;
			},
			getIssueType(key) {
				axios.get('api/jira/issuetype')
				this.loading = false;
			},
			listCommands() {
				this.display.push({
					type: 'default',
					text: this.tableCommands()
				});
				this.loading = false;
			},
			clear() {
				this.display = [];
				this.loading = false;
			},
			scrollCommands() {
				this.vector++;
				if (this.vector > this.executeds.length - 1)
					this.vector = 0;

				this.command = this.executeds[this.vector];
			},
			listProjects(param) {
				axios.get(`api/jira/projects/${param}`).then(res => {
					this.display.push({
						type: 'danger',
						text: JSON.stringify(res.data, null, 2)
					})
				});
				this.loading = false;
			},
			cancel() {
				this.source.cancel();
			},
			createIssues() {
				axios.post('api/issue/create', {}).catch( error => {
					let err = error.response;
					this.display.push({
						type: 'danger',
						text: err.data.message
					})
				});

				this.loading = false;
			},
			isHtml(str) {
				return str.match(/<[a-z][\s\S]*>/i);
			}
		}
	})
</script>
<script>
	$(".terminal").click(function() {
		$("#prompt").focus();
	});
</script>
@endpush