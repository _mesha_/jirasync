<?php
namespace App\Traits;

use Illuminate\Database\Eloquent\SoftDeletes;

trait AppTrait {
  use SoftDeletes;
}