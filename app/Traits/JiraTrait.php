<?php

namespace App\Traits;

use AtlassianConnectCore\Http\Clients\JWTClient;
use AtlassianConnectCore\Repositories\TenantRepository;

trait JiraTrait {

  private $tenant;
  private $client;

  public function connect() {
      $this->tenant = app(TenantRepository::class);
      $this->client = new JWTClient($this->tenant->findById(2));
      
      return $this->client;
  }
}