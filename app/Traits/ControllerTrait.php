<?php

namespace App\Traits;

trait ControllerTrait {

  private $tenant;
  public $project;
  private $client;

  public function jira() {
      $this->tenant = app(TenantRepository::class);
      $this->client = new JWTClient($this->tenant->findById(2));
      $this->project = $this->getProjectByName('OdontoServ');
  }
}