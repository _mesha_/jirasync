<?php

namespace App\Calling;

use Illuminate\Database\Eloquent\Model;

use App\Jira\Status as JiraStatus;

class Status extends Model
{

    protected $table = 'calling_statuses';

    protected $fillable = [
        'name',
        'code',
        'jira_id',
    ];

    public function JiraStatus() {
        return $this->belongsTo(JiraStatus::class, 'jira_id', 'jira_id');
    }
}
