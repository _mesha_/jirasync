<?php

namespace App\Calling;

use Illuminate\Database\Eloquent\Model;
use App\Calling\CallingType;
use App\Calling\Status;

class Calling extends Model
{
    protected $fillable = [
    	"aberto_em",
        "dat_abertura",
        "num_chamado",
        "titulo",
        "status",
        "cod_status",
        "descricao",
        "tipo",
        "responsavel",
        "sistema",
        "dat_estouro_sla",
        "dat_final_sla",
        "num_tempo_chamado",
        "tempo_resposta_chamado",
        "dat_fechamento",
        "data_fechamento",
        "task_id",
        "membro",
        "txt_conclusao",
        "md5hash",
        "created_at",
        "updated_at",
    ];

    /**
     * Relation belongsTo \App\Status
     * 
     * @return \Illuminate\Database\Eloquent\BelongsTo
     */
    public function Status()
    {
        return $this->belongsTo(Status::class, 'cod_status', 'cod');
    }
    
    /**
     * Relation belongsTo \App\Status
     * 
     * @return \Illuminate\Database\Eloquent\BelongsTo
     */
    public function Type()
    {
        return $this->belongsTo(CallingType::class, 'cod_status', 'cod');
    }
    
    /**
     * Relation hasOne \App\Issue
     * 
     * @return \Illuminate\Database\Eloquent\HasOne
     */
    public function Issue()
    {
        return $this->hasOne(\App\Jira\Issue::class, 'calling_number', 'num_chamado');
    }

    public function scopeNumber($q, $calling_number) 
    {
        return $q->whereNumChamado($calling_number);
    }
    
    public function scopeOpen($q) 
    {
        return $q->whereNotIn('cod_status', [2,3]);
    }

    public function scopeNew($query)
    {
        return $query->where('is_new', true);
    }


}
