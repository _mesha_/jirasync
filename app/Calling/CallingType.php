<?php

namespace App\Calling;

use Illuminate\Database\Eloquent\Model;
use App\Jira\IssueType;

class CallingType extends Model
{
    protected $fillable = [
    	'name',
        'issue_type_id',
    ];

   /**
	* Get the App\Jira\IssueType.
	*
	* @return Illuminate\Database\Eloquent\Relations\BelongsTo
	*/
	public function IssueType()
	{
		return $this->belongsTo(IssueType::class, 'issue_type_id', 'jira_id');
	}
}
