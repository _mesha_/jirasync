<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Calling\Calling;

class CallingController extends Controller
{
    public function index(Request $request) 
    {
    	return response()->json(Calling::new()->get(), 200);
    }
}
