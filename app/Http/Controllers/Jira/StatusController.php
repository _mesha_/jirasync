<?php

namespace App\Http\Controllers\Jira;

use App\Status;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\JiraTrait as Jira;

class StatusController extends Controller
{

    use Jira;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $ret = $this->connect()->get('rest/api/2/status?expand=transitions.fields', [
          'query' => [
              'expand' => 'body.storage'
          ]
      ]);
      
      return response()->json($ret, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Status  $Status
     * @return \Illuminate\Http\Response
     */
    public function show(Status $Status)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Status  $Status
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Status $Status)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Status  $Status
     * @return \Illuminate\Http\Response
     */
    public function destroy(Status $Status)
    {
        //
    }
}
