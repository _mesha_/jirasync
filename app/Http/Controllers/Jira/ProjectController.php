<?php

namespace App\Http\Controllers\Jira;

use App\Jira\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\JiraTrait as Jira;

class ProjectController extends Controller
{

    use Jira;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ret = $this->connect()->get('rest/api/2/project', [
            'query' => [
                'expand' => 'body.storage'
            ]
        ]);
        
        return response()->json($ret, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show($key)
    {
        
        $ret = $this->connect()->get("rest/api/2/project/{$key}", [
            'query' => [
                'expand' => 'body.storage'
            ]
        ]);

        return response()->json($ret, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        //
    }
}
