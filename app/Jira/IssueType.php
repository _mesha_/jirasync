<?php

namespace App\Jira;

use Illuminate\Database\Eloquent\Model;

class IssueType extends Model
{
    protected $fillable = [
    	"self", // "https://mesha.atlassian.net/rest/api/2/issuetype/10001"
	    "id", // "10001"
	    "description", // "Stories track functionality or features expressed as user goals."
	    "iconUrl", // "https://mesha.atlassian.net/secure/viewavatar?size=xsmall&avatarId=10315&avatarType=issuetype"
	    "name", // "Story"
	    "avatarId", // 10315
    ];
}
