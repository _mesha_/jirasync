<?php

namespace App\Jira;

use Illuminate\Database\Eloquent\Model;
use \App\Traits\AppTrait;

class Project extends Model
{
    use AppTrait;

    protected $table = 'projects';

    protected $fillable = [
        "self",
        "jira_id",
        "key",
        "name",
        "avatar_url"
    ];

    public function scopeAutomatic($query)
    {
        return $query->whereAutomatic(true);
    }
}

