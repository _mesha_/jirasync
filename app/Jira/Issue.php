<?php

namespace App\Jira;

use Illuminate\Database\Eloquent\Model;
use \App\Traits\AppTrait;

class Issue extends Model
{

    use AppTrait;

    protected $fillable = [
        'key',
        'self',
        'jira_id',
        'summary',
        'description',
        'calling_number',
        'issuetype',
        'project_id'
    ];

    /**
     * Relation belongsTo \App\Calling
     * 
     * @return \Illuminate\Database\Eloquent\BelongsTo
     */
    public function Calling()
    {
        return $this->belongTo(\App\Calling\Calling::class, 'num_chamado', 'calling_number');
    }

    /**
     * Scope defined to consulting issues by calling number
     * 
     * @param Illuminate/Database/Eloquent/Builder $q
     * @param string $calling_number
     * @return Illuminate/Database/Eloquent/Builder 
     */
    public function scopeCallingNumber($q, $calling_number)
    {
        return $q->where('calling_number', $calling_number);
    }

    public static function boot() {
        parent::boot();

        static::created(function($issue) {
            \DB::table('callings')->where('num_chamado', $issue->calling_number)->update(['is_new' => false, 'needs_update' => true]);
        });
    }

}
