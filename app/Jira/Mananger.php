<?php

namespace App\Jira;

use Illuminate\Database\Eloquent\Model;
use App\Jira\Project;
use App\Calling\Status as CallingStatus;
use App\Jira\Issue;
use App\Traits\JiraTrait as Jira;

class Mananger extends Model
{
    use Jira;

    public $project;

    /**
     * creates issue on jira
     * 
     * @param array $data
     * @return void
     */
    public function createIssue(array $data)
    {
        
    }

    /**
     * Update an issues based on issue key
     * 
     * @param array $data
     * @return void
     */
    public function updateIssue($issue, $call)
    {

        $status = CallingStatus::with('jiraStatus')->where('cod', $call->cod_status)->first();

        $data = [
            "update"=> [
                "comment"=> [
                    [
                        "add"=> [
                            "body"=> "Alteração de dados pelo JiraSync"
                        ]
                    ]
                ]
            ],
            "transition"=> [
                "id"=> $status->jiraStatus->transitio_id
            ],
            "fields" => [
                "summary" => time() . " {$call->num_chamado} {$call->titulo}",
                "description" => time() . " " . $this->removeSpecialCharacters($call->descricao),
            ]
        ];

        $this->connect()->put("rest/api/2/issue/{$issue->key}", $data);

        $data = [
            "update"=> [
                "comment"=> [
                    [
                        "add"=> [
                            "body"=> "Alteração de status pelo JiraSync"
                        ]
                    ]
                ]
            ],
            "transition"=> [
                "id"=> $status->jiraStatus->transitio_id
            ],
            "fields" => [
                "resolution"=> [
                    "name" => "Fixed"
                ]
            ]
        ];

        $this->connect()->put("rest/api/2/issue/{$issue->key}/transitions", $data);

    }

    /**
     * Returns all statuses from Jira
     * 
     * @return array
     */
    public function getStatues() : array
    {

        $ret = $this->connect()->get("rest/api/2/project/OD/statuses", [
            'query' => [
                'expand' => 'body.storage'
            ]
        ]);

        return reset($ret)['statuses'];
    }

    public function example() 
    {
        
        $ret = $this->connect()->get("rest/api/3/workflow/rule/config", [
            'query' => [
                'expand' => 'body.storage'
            ]
        ]);
        return $ret;
    }

    /**
     * Returns all transitions tasks based on a issue key
     * 
     * @return array
     * @param string $key
     */
    public function getTransitions(string $key) : array
    {
        return $this->connect()->get("rest/api/2/issue/{$key}/transitions?expand=transitions.fields", [
            'query' => [
                'expand' => 'body.storage'
            ]
        ]);
    }

    public function getIssue(string $key = '') : array
    {
        return $this->connect()->get("rest/api/2/issue/{$key}", [
            'query' => [
                'expand' => 'body.storage'
            ]
        ]);
    }

    /**
     * Returns the default project
     * 
     * @param string $name
     * @return \App\Jira\Project 
     */
    private function getProject(string $name) : Project
    {
        return Project::automatic()->first();
    }

    /**
     * Returns a \App\Jira\Status based on id
     * 
     * @param int $id
     * @return \App\Jira\Status 
     */
    private function getStatusById(int $id) : CallingStatus
    {
        return CallingStatus::with('jiraStatus')
            ->where('status_id', $id)
            ->first()   
        ;
    }

    public function getIssueType()
    {
        $this->connect()->get("rest/api/2/issue", [
            'query' => [
                'expand' => 'body.storage'
            ]
        ]);
    }

    /**
     * Returns a cleared string
     * 
     * @param string $string
     * @return void
     */
    public function removeSpecialCharacters(string $string) : string
    {
        return strip_tags(mb_convert_encoding(html_entity_decode(strtolower($string)), 'UTF-8', 'auto'));
    }

    private function success($response) {
        return array_key_exists('error', $response);
    }
}
