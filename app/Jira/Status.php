<?php

namespace App\Jira;

use Illuminate\Database\Eloquent\Model;
use App\Traits\AppTrait;

class Status extends Model
{
    use AppTrait;

    protected $fillable = [
      'name',
      'jira_id',
      'self',
      'transition_id'
    ];

    /**
     * Get the App\Jira\Transition that owns a App\Jira\Status.
     * 
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Transition()
    {
      return $this->belongsTo(Transition::class, 'transition_id', 'jira_id');
    }
}
