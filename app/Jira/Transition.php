<?php

namespace App\Jira;

use Illuminate\Database\Eloquent\Model;
use App\Traits\AppTrait;

class Transition extends Model
{

    use AppTrait;

    protected $fillable = [
        'jira_id',
        'name',
        'message',
    ];
}
