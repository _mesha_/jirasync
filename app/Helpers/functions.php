<?php

function createlog(string $description) : \App\Log
{
  return \App\Log::create(['description' => $description]);
}

function getlog(int $qty = 20) : \Illuminate\Database\Eloquent\Collection
{
  return \App\Log::limit($qty)->orderBy('created_at', 'desc')->get();
}