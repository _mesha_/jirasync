<?php

namespace App\Listeners\Jira\Project;

use App\Events\Jira\Project;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class Created
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Project  $event
     * @return void
     */
    public function handle(Project $event, Tenant $tenant, Request $request)
    {
        \Storage::put('text.txt', json_encode($request->all()));
    }
}
