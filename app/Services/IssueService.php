<?php

namespace App\Services;

use App\Calling\Calling;
use App\Jira\Project;
use App\Calling\CallingType;
use App\Traits\JiraTrait as Jira;
class IssueService {

  use Jira;

  public function createIssue()
  {
    $res = Calling::where('is_new', true)->limit(1)->get();
    $project = Project::automatic()->first();
    $result = [
      'errors' => [],
      'created' => []
    ];
    
    $res->each(function($call) use ($project, &$created) {
      
      $callingType = CallingType::where('name', $call->tipo)->with('issueType')->first();      
      
      $data = [
        "project" => [
          'id'=>$project->jira_id,
        ],
        "summary" => $call->titulo,
        "description" => strip_tags(mb_convert_encoding(html_entity_decode(strtolower($call->descricao)), 'UTF-8', 'auto')),
        "issuetype" => [
          "id" => $call->issueTypeId
        ],
        "labels" => [
          "bugfix",
          "blitz_test"
        ],
      ];

      $issue = $this->connect()->post('rest/api/2/issue/', ["fields"=>$data] );
      
      if ( array_key_exists('error', $issue)) {
        $result['errors'][] = $issue;
      } else {
        $result['created'][] = $issue;
        $call = \App\Calling\Calling::where('num_chamado', $issue->calling_number)->update(['is_new'=>false]);
      }
    });

    return $created;
  }

  public static function getIssueType($key)
  {
    $jm = new \App\Jira\Mananger();
    return $jm->getIssueType($key);
  }
}